# StationBot for Discord

---

StationBot is a simple and efective bot made for discord that can do many features that 
bots today have, but at no cost and it's open source.

---

## More Info

[Click here for more information about **StationBot**](https://bot.perezbueno.xyz/stationbot)

## Invite the bot
[Click here to invite the bot](https://discord.com/api/oauth2/authorize?client_id=871786387626668122&permissions=536870911991&scope=bot%20applications.commands)
Invite to the testing bot.
