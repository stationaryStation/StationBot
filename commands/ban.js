const { SlashCommandBuilder } = require('@discordjs/builders')

module.exports = {
    data: new SlashCommandBuilder()
        .setName('ban')
        .setDescription('Bans the mentioned user')
        .addUserOption(option => option.setName('user')
                       .setDescription('User Mention')
                       .setRequired(true)
                      ),
    async execute(interaction){
        const user = interaction.options.getUser('user');
        if (!user){
            interaction.reply('Does the user exist and it\'s on the server?\nPlease try again and make sure that you typed the user correctly.')
        } else {
            interaction.guild.members.ban(user);
        }


    }
}
