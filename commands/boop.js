// Require discord slash command builder
const { SlashCommandBuilder } = require('@discordjs/builders')

module.exports = {
  data: new SlashCommandBuilder()
  .setName('boop')
  .setDescription('Pings the user you mentioned')
  .addUserOption(option => option.setName('user')
    .setDescription('User to Ping.')
    .setRequired(true)),

  async execute(interaction){
    const userToPing = interaction.options.getUser("user").toString();
    interaction.reply(`${userToPing}, ${interaction.user} pinged you!`);
  }
}
