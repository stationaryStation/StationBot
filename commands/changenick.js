const { SlashCommandBuilder } = require('@discordjs/builders');

module.exports = {
    data: new SlashCommandBuilder()
        .setName('changenick')
        .setDescription('Changes your nickname')
        .addStringOption(option => option.setName('nickname')
                         .setDescription('Replace with this Nickname')
                        ),
    async execute(interaction){
        const nickname = interaction.options.getString('nickname');
        if (interaction.member.roles.highest){
            const sent = await interaction.reply({content: "Changing Nickname...", fetchReply: true});
            interaction.editReply("It seems that you can do that by yourself :neutral_face:")
        } else if (nickname === interaction.user.nickname){
            const sent = await interaction.reply({content: "Changing Nickname...", fetchReply: true});
            interaction.editReply("It seems that the specified nickname is the same as the one you want to change.")
        } else{
            const sent = await interaction.reply({content: "Changing Nickname...", fetchReply: true});
            interaction.member.setNickname(nickname, 'Used /nickname')
            interaction.editReply("**Changed nickname successfuly!**")
        }
    }
};
