const { SlashCommandBuilder } = require('@discordjs/builders')

module.exports = {
    data: new SlashCommandBuilder()
        .setName('kick')
        .setDescription('Kicks the mentioned user')
        .addUserOption(option => option.setName('user')
                       .setDescription('User to kick')),
    async execute(interaction) {
        const user = interaction.options.getUser('user');
        if (!user) {
            interaction.reply({content: "Is the user here and alive? My sensors don't detect him.\n Please try again.", ephemeral: true})
        } else {
            interaction.guild.kick(user);
        }

    }
};
