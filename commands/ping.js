const { SlashCommandBuilder } = require('@discordjs/builders');
module.exports = {
  data: new SlashCommandBuilder()
        .setName('ping')
        .setDescription("Check bot's ping"),
    name: "ping",
    description: "Says bot's ping ",
  async aexecute(interaction){
    const sent = await interaction.reply({ content: 'Pinging...', fetchReply: true});
    sent.editReply(`**Pong!**\nBot's Ping: ${sent.createdTimestamp - interaction.createdTimestamp}ms`);
  },
    execute(message, Client){
        message.channel.send(`Websocket heartbeat: ${Client.ws.ping}ms\nRoundtrip latency: Pinging...`).then(sent => {
            sent.edit(`Websocket heartbeat: ${Client.ws.ping}ms\nRoundtrip latency: ${sent.createdTimestamp - message.createdTimestamp}ms`);
        })
    }
}
