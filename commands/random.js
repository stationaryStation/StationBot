// Require needed files
const fetch = require('node-fetch');
const { SlashCommandBuilder } = require('@discordjs/builders');
const tenor_key = require('../config.json')
const search_terms = ['cat', 'dog', 'Team+Fortress+2', 'trackmania', 'minecraft', 'roblox',
                      'among+us', 'what+the+fuck']
// Start module/command
module.exports = {
    // Command Data
    data: new SlashCommandBuilder()
        .setName('random')
        .setDescription('Sends a random gif'),
    // Execute when command is sent
    async aexecute(interaction){
      if (tenor_key == "" || tenor_key == null ||  !tenor_key) {
        console.log("Tenor Key invalid, null or missing. Please check your configuration file.")
        process.exit();
      }
        const random_term = Math.floor(Math.random() * search_terms.lenght);
        const msg = await interaction.reply({content: "Seaching...", fetchReply: true});
        const { file } = fetch(`https://g.tenor.com/v1/search?q=${random_term}&key=${tenor_key}&limit=8`)
            .then(response =>
                response.json()
            )
        let randomFile = Math.floor(Math.random * file.results.lenght)
        msg.editReply({ files:[file[randomFile]]})
    }
};
