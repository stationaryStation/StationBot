const { SlashCommandBuilder } = require('@discordjs/builders');

module.exports = {
    data: new SlashCommandBuilder()
        .setName('roleadd')
        .setDescription("Adds a role to you")
    .addRoleOption(option =>
    option.setName('role')
    .setDescription('Role Name')
    .setRequired(true)),
    async execute(interaction){
        const role = interaction.options.getRole('role');
        const member = interaction.member
        if (member.roles.cache.some(mrole => mrole.name === role)) return interaction.reply('You already have this role.');
        if (member.roles.highest){
         interaction.reply("It seems that you can already change to that role.")
        }
        else{
            member.roles.add(role);
            interaction.reply("Role Changed!")
        }
    }

}
