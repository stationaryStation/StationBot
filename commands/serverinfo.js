// Require Discord.js library and builder for slash commands
const { MessageEmbed } = require('discord.js');
const { SlashCommandBuilder } = require('@discordjs/builders');

//Start module for command
module.exports = {
  data: new SlashCommandBuilder()
        .setName('serverinfo')
        .setDescription('Lists current server information.'),
  async execute(interaction) {
    const infoEmbed = new MessageEmbed()
      .setColor('#FF0057')
      .setTitle('Server Info')
      .setDescription('Information:')
      .addFields(
        {name: 'Name', value: `${interaction.guild.name}`},
        {name: 'Member Count', value:`${interaction.guild.memberCount}`},
        {name:'Created at:', value:`${interaction.guild.createdAt}`},
        {name:'Server Boosts:', value:`${interaction.guild.premiumSubscriptionCount}`}
      )
    interaction.reply({ embeds: [infoEmbed]});
  }
}
