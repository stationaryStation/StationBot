// Require discord.js base
const { Client, Intents, Collection } = require('discord.js');
const { Token } = require('./config.json');

// Require other libraries
const fs = require('fs');

// Create a new discord client
const client = new Client({intents: [Intents.FLAGS.GUILDS]});

// Create a new Discord Collection
client.commands = new Collection();

//Add commands to the command handler
const commandFiles = fs.readdirSync('./commands/').filter(file => file.endsWith('.js'));

for (const file of commandFiles) {
  const command = require(`./commands/${file}`);
  // Set a new item in the Collection we added earlier
  // With the key as the command name and the value as the exported module
  client.commands.set(command.data.name, command)
}

// Alert the user through console when the bot is ready
client.once('ready', () => {
  console.log('Client Ready!');
});
// Wait for messages then check if it is a valid command
client.on('interactionCreate', async interaction => {
  if (!interaction.isCommand()) return;
    isprefixed = false;

  const command = client.commands.get(interaction.commandName);
  if (!command) return;

  try{
    await command.aexecute(interaction, Client);
  } catch (error){
    await interaction.reply({content: `There was an error while executing this command\nError Stack: \`\`\`${error}\`\`\``, ephemeral: true});
  }

});

// Login with the provided token @ config.json
client.login(Token);
